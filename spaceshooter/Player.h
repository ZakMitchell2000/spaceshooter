#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
class Player
{
public:
// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u screenSize);
	
	void Input();
	void Update(sf::Time frameTime);
	void Draw(sf:: RenderWindow& gameWindow);

private:

	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;

	sf::Vector2u screenSize;

};

