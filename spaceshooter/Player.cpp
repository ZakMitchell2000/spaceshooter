#include "Player.h"
Player::Player(sf::Texture& playerTexture, sf::Vector2u newScreenSize)
{

	screenSize = newScreenSize;

	sprite.setTexture(playerTexture);
	sprite.setPosition(
		(screenSize.x / 2 - playerTexture.getSize().x / 2)*0.25f,
		screenSize.y / 2 - playerTexture.getSize().y / 2
	);

	velocity.x = 0.0f;
	velocity.y = 0.0f;
	speed = 300.0f;
}
void Player::Input()
{
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}
}
void Player::Update(sf::Time frameTime)
{
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	// LEFT
	if (newPosition.x < 0)
		newPosition.x = 0;
	// RIGHT
	if (newPosition.x + sprite.getTexture()->getSize().x > screenSize.x)
		newPosition.x = screenSize.x - sprite.getTexture()->getSize().x;
	// TOP
	if (newPosition.y < 0)
		newPosition.y = 0;
	// BOTTOM
	if (newPosition.y + sprite.getTexture()->getSize().y > screenSize.y)
		newPosition.y = screenSize.y - sprite.getTexture()->getSize().y;


	// Move the player
	sprite.setPosition(newPosition);
}
void Player::Draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(sprite);
}
