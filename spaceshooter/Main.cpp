// Library Includes	
// Library needed for using sprites, textures, and fonts
#include <SFML/Graphics.hpp>
// Library needed for playing music and sound effects
#include <SFML/Audio.hpp>
// Library for manipulating strings of text
#include <string>
// Library for handling collections of objects
#include <vector>
#include "Player.h"
#include "Star.h"

int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Space Shooter", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;

	// Player Setup
	sf::Texture playerTexture;
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	Player playerInstance(playerTexture, gameWindow.getSize());

	// Stars
	sf::Texture starTexture;
	starTexture.loadFromFile("Assets/Graphics/star.png");
	std::vector<Star> stars;
	int numStars = 5;
	for (int i = 0; i < numStars; ++i)
	{
		stars.push_back(Star(starTexture, gameWindow.getSize()));
	}

	// Game Loop
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed

			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		} // End event polling loop
		playerInstance.Input();


		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();

		playerInstance.Update(frameTime);

		// Update the stars
		for (int i = 0; i < stars.size(); ++i)
		{
			stars[i].Update(frameTime);
		}

		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color(15, 15, 15));

		// Draw game objects
		playerInstance.Draw(gameWindow);

		// Draw the stars
		for (int i = 0; i < stars.size(); ++i)
		{
			stars[i].DrawTo(gameWindow);
		}

		// Display the window contents on the screen
		gameWindow.display();



	} // End of Game Loop
	return 0;
}